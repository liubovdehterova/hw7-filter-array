/*
1. Опишіть своїми словами як працює метод forEach.
forEach перебирає масив, виконуючи з кожним елементом масива описані дії, нічого не повертає
2. Як очистити масив?
Перназначити змінну на пустий масив
через довжину масива(arr.length = 0;)
3. Як можна перевірити, що та чи інша змінна є масивом?
Array.isArray()
*/

function filterBy(arr, type) {
    let newArr = arr.filter(
        (e) => {
            return type === 'null' ? e !== null : typeof e !== type;
    });
    return newArr;
}

console.log(filterBy(['hello', 'world', 23, '23', null, undefined], 'null'));
